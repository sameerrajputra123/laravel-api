-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2019 at 11:53 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_19_063249_create_products_table', 1),
(5, '2019_12_19_063315_create_reviews_table', 1),
(6, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(7, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(8, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(9, '2016_06_01_000004_create_oauth_clients_table', 2),
(10, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('cf32e30da89d0cb14d4b073be5ebab6d4f96938fca5d034e0c3a605d6e7c01d5528430601c6307f3', 1, 2, NULL, '[]', 0, '2019-12-20 01:41:01', '2019-12-20 01:41:01', '2020-12-20 07:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'restful Personal Access Client', 'OGPc2JXcYAEJR5mqktIizfGw06GWo34UQuiLUBEr', 'http://localhost', 1, 0, 0, '2019-12-19 05:57:58', '2019-12-19 05:57:58'),
(2, NULL, 'restful Password Grant Client', 'knN5KbvKRDGBN1hJtI2njE5j9ZvEQHmlEgLBWyzb', 'http://localhost', 0, 1, 0, '2019-12-19 05:57:58', '2019-12-19 05:57:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-12-19 05:57:58', '2019-12-19 05:57:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('413d0b9bad130582b9d74c0322bd2660299f52b098518f418374c4530f8ff0cf7049d03cc59de400', 'cf32e30da89d0cb14d4b073be5ebab6d4f96938fca5d034e0c3a605d6e7c01d5528430601c6307f3', 0, '2020-12-20 07:26:02');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `price`, `stock`, `discount`, `created_at`, `updated_at`) VALUES
(121, 'ex', 'Veniam accusamus nihil modi suscipit reprehenderit aperiam non. Aperiam ea consequatur amet eum incidunt id omnis id. Illum quod repudiandae sint magni necessitatibus.', 619, 0, 8, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(122, 'et', 'Quod maiores molestias voluptatem voluptatem. Rerum aliquid vel sed molestias. Molestiae aspernatur ipsum quam ab laudantium sed dolorum.', 723, 9, 28, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(123, 'nulla', 'Maiores hic modi quia veniam ut officia inventore explicabo. Ut maxime explicabo voluptates culpa rerum totam. Nam vel dolor culpa.', 396, 2, 12, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(124, 'doloribus', 'Vero hic beatae aut atque. Ad et reprehenderit cum cupiditate repellendus excepturi reiciendis et. Fugit magni eaque consequatur tempora et porro eum.', 461, 7, 6, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(125, 'alias', 'Consequuntur molestias ut eum aliquam quis voluptatem quidem ducimus. Omnis cum dolores odit excepturi quia. Enim et nihil et animi error corporis numquam. Accusantium molestiae ut eum velit quam.', 778, 4, 7, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(126, 'sit', 'Rerum quia aliquam et consectetur. Omnis sunt voluptatem quas ullam. Odit culpa et provident incidunt et error totam nisi. Et corporis qui aperiam consequuntur. Non sunt quibusdam alias doloribus rerum.', 145, 8, 13, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(127, 'distinctio', 'Hic eligendi est vel rerum praesentium. Architecto sequi eveniet totam est. Enim voluptate sequi iusto sed dolores quia adipisci quod. Ea qui enim voluptatem porro earum ut voluptatem.', 586, 3, 12, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(128, 'omnis', 'Beatae nulla rem voluptate est labore vero voluptatibus nihil. Est dolores autem ipsam vel.', 785, 8, 4, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(129, 'inventore', 'Cumque debitis ratione quo autem. Sint rerum eveniet impedit aliquid sit qui assumenda. Voluptas quia enim rerum asperiores adipisci.', 677, 6, 7, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(130, 'quas', 'Ducimus fugit impedit ut voluptatem itaque fugiat. Inventore et laudantium ea dignissimos odit consequatur numquam ex. Consectetur suscipit architecto et voluptas.', 336, 9, 14, '2019-12-19 02:29:59', '2019-12-19 02:29:59'),
(131, 'vitae', 'Nihil fugiat repudiandae velit et. Modi facilis sed dolorum dicta dignissimos. Voluptatem quia harum aut in iusto quaerat error.', 816, 6, 10, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(132, 'velit', 'Quibusdam sit aut voluptatibus consequuntur possimus sed. Sit esse aut libero inventore sed dolores atque et. Non id voluptas dolorum.', 706, 5, 30, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(133, 'accusantium', 'Non error tempora ut praesentium enim quam fugiat. Rem est quia nostrum dolores atque est temporibus. Aut consectetur debitis unde et voluptas laudantium.', 860, 9, 2, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(134, 'velit', 'Quam consequatur aperiam voluptas alias nobis consequatur. Occaecati velit rerum libero nobis porro non et. Et et magni qui dolorem voluptas autem repellat eveniet. Excepturi consectetur sed fugit voluptas qui cumque sunt.', 863, 5, 27, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(135, 'et', 'Commodi maiores id sunt neque ipsa voluptatum. Est ducimus est nihil hic. Voluptatibus molestias nulla est culpa.', 410, 1, 7, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(136, 'ducimus', 'Molestiae perferendis accusantium aliquid provident consectetur rem. Autem consequuntur perferendis deleniti nobis et autem alias. Rerum omnis corrupti et repellendus iste ut. A consectetur est nihil quos aut voluptate error ut. Qui quo harum et eum ea beatae amet eveniet.', 965, 1, 18, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(137, 'quia', 'Illo quo asperiores minima aut. Sit enim ea aspernatur dolore. Quibusdam veniam consectetur rerum iste hic sed corporis sequi. Quia autem nesciunt aliquam ut sunt blanditiis totam ut.', 992, 5, 9, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(138, 'natus', 'Repellat quasi quis at omnis. Ratione tempore dolorem nulla omnis dolor. Sequi doloribus recusandae ab consequatur nihil culpa possimus atque. Ipsam id rerum rerum eveniet.', 791, 4, 5, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(139, 'in', 'Voluptatibus aut aut pariatur enim. Necessitatibus necessitatibus eum dolorem praesentium quae a. Neque cupiditate assumenda inventore delectus laborum. Iste laudantium ut qui impedit accusantium assumenda sint est.', 587, 9, 12, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(140, 'inventore', 'Aperiam consectetur fugit quia id voluptas officia. Laudantium maiores excepturi impedit atque labore vel. Repudiandae nulla dolorem laborum quae perferendis dignissimos dolores modi. Facilis veritatis expedita laboriosam.', 813, 4, 13, '2019-12-19 02:30:00', '2019-12-19 02:30:00'),
(141, 'blanditiis', 'Nesciunt eum tempora praesentium repellendus repellat. Vitae quibusdam doloremque et distinctio. Ea dolores commodi ipsa et et. Cupiditate quibusdam quos veniam laboriosam quo explicabo aut.', 639, 1, 27, '2019-12-19 02:30:38', '2019-12-19 02:30:38'),
(142, 'pariatur', 'Quasi atque non et cumque ut. Sit cum saepe rerum et non aut natus. Deserunt fugiat quidem velit cumque.', 405, 3, 4, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(143, 'rerum', 'Animi praesentium iste reprehenderit tempora. Voluptatem dolor qui ut et esse.', 854, 7, 5, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(144, 'nostrum', 'Ut debitis aut quisquam et a quia. In explicabo non officiis impedit amet et itaque. Dolorem quae perferendis atque sed hic dolore.', 217, 4, 9, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(146, 'ut', 'Aliquam aspernatur vitae hic consequatur odio rem. Aut voluptates voluptas quisquam. Ipsam a mollitia ut.', 948, 6, 15, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(147, 'odit', 'Deserunt voluptates corporis aut dolores sit facilis sint rerum. Non aut non et modi officiis aut omnis. Iste laborum veniam facere iusto nostrum qui odio non.', 131, 9, 7, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(148, 'debitis', 'Id laborum mollitia occaecati voluptatem aut. Laborum laboriosam voluptatem impedit aliquam dolorem. Eum perspiciatis assumenda tempore. Ab fugiat dolorem et.', 429, 3, 2, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(149, 'aperiam', 'Sint omnis qui minima dolor modi. Ducimus velit illum et eligendi. Rerum commodi fuga dignissimos hic saepe velit odio. Debitis ducimus omnis et voluptas sit.', 917, 8, 8, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(150, 'maiores', 'Voluptas voluptas ut atque rem nihil repudiandae architecto ratione. Labore omnis soluta ex dicta non delectus expedita facilis. Voluptas dicta quis et aut illum. Voluptatem expedita nisi modi ipsam.', 552, 9, 15, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(151, 'reprehenderit', 'Est veniam sed est dolor ut in qui. Recusandae illo accusamus quas. Dignissimos consequatur vitae similique repellat. Exercitationem voluptate quisquam magni et. Porro aut ut aut voluptatem.', 693, 5, 27, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(152, 'id', 'Id omnis mollitia adipisci qui quis similique. Debitis qui aut quod nostrum et magnam veniam. Sequi soluta nesciunt architecto provident eligendi.', 773, 1, 28, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(153, 'sed', 'Voluptas mollitia dolorem eum deserunt minima voluptates excepturi dolore. Accusantium eum accusamus qui sunt recusandae. Molestiae sint veritatis hic sunt est vitae.', 630, 6, 28, '2019-12-19 02:30:39', '2019-12-19 02:30:39'),
(154, 'ea', 'Debitis error eligendi asperiores distinctio ullam esse. Velit in nostrum pariatur. Non aliquam tempore eum quia ut sint. Totam occaecati corporis accusamus possimus.', 642, 9, 3, '2019-12-19 02:30:40', '2019-12-19 02:30:40'),
(155, 'quia', 'Quia aliquid quisquam ipsa rem ducimus ipsa quidem qui. Quia sed maxime eaque hic accusamus delectus. Molestiae est dolorem doloremque dicta ad praesentium.', 265, 0, 11, '2019-12-19 02:30:40', '2019-12-19 02:30:40'),
(156, 'labore', 'Consequatur officia minima dolor aut laboriosam occaecati ad eos. Magnam laboriosam perferendis sint saepe. Quaerat similique aut a.', 791, 8, 14, '2019-12-19 02:30:40', '2019-12-19 02:30:40'),
(157, 'deserunt', 'Numquam in nihil at similique. Expedita sit vero voluptas molestiae odio quibusdam. Ipsam totam at fugit ea aliquam dolor. Velit consequatur laboriosam velit est.', 502, 6, 27, '2019-12-19 02:30:40', '2019-12-19 02:30:40'),
(158, 'quis', 'Labore omnis et omnis odio. Eius ut quidem eum. Tempore deserunt cum tempore amet dolorem animi delectus. Officiis qui dolores harum non et sequi.', 694, 8, 9, '2019-12-19 02:30:40', '2019-12-19 02:30:40'),
(159, 'ut', 'Minima et voluptas ea voluptas dolore eos eos inventore. Voluptatem officia rerum dolore cupiditate omnis numquam. Voluptas adipisci quis qui eos aut quis rerum.', 331, 1, 27, '2019-12-19 02:30:40', '2019-12-19 02:30:40'),
(160, 'autem', 'Quae quo voluptatem a et recusandae quaerat. Nostrum nesciunt et culpa corrupti excepturi voluptatem rerum qui. In et accusantium quis excepturi.', 533, 1, 27, '2019-12-19 02:30:40', '2019-12-19 02:30:40');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `star` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `product_id`, `customer`, `review`, `star`, `created_at`, `updated_at`) VALUES
(1, 131, 'Janelle Jenkins', 'Odio praesentium omnis tempora commodi. Magnam qui voluptas minus ipsam voluptates. Quis omnis omnis repellendus minima rem. Et ut nam sed sapiente.', 2, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(2, 151, 'Ezekiel Christiansen II', 'Tenetur enim est eveniet vel corporis est cupiditate. At ea vel explicabo temporibus et. Culpa rerum corrupti aut velit quis omnis laborum.', 5, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(3, 131, 'Leonard Trantow', 'Blanditiis soluta laudantium et. Error sint sit cupiditate aliquid est dolore voluptatem est. Sunt non eveniet qui nostrum.', 2, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(4, 128, 'Mr. Shane Casper MD', 'Eum dolor assumenda quod eos nemo dolor culpa. Reprehenderit omnis reprehenderit ipsum tempora consequatur. Quasi officiis eius dolores nam incidunt perspiciatis. Voluptas illo et neque.', 4, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(5, 128, 'Quinten Konopelski', 'Est enim voluptates in et nam. Error sed autem quibusdam sunt. Vitae debitis amet temporibus temporibus pariatur nisi. Quidem repellat officia earum.', 1, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(6, 148, 'Dr. Lucy Zieme', 'Ut aut quia cumque eveniet fuga ipsa. Eos quo commodi aut voluptatem in aut consequuntur. Rerum magnam molestias ipsa incidunt. Quia voluptates distinctio suscipit voluptatem ad iure deleniti.', 3, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(7, 129, 'Lexie Little', 'Quos dignissimos velit pariatur. Perferendis ipsum nam perferendis molestiae ipsa. Sit quo quo voluptate ut suscipit pariatur nemo. Modi ut illo alias.', 4, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(8, 159, 'Dr. Anthony Donnelly', 'Ad autem omnis qui similique repellendus voluptatibus velit. Sequi recusandae rerum facere quis tenetur. Sit ipsum voluptatem error rerum. Voluptas nesciunt velit dolorum.', 1, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(9, 152, 'Raoul Glover', 'Ea doloremque unde tempora voluptate aliquam quasi amet. Sed mollitia reprehenderit dignissimos ut quia. Sit possimus deleniti et ea voluptatem.', 2, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(10, 150, 'Mrs. Lilla Eichmann', 'Maxime aperiam neque odit vel ut. Itaque unde maxime magni delectus eveniet ut magni sapiente.', 2, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(11, 126, 'Chandler Gutmann', 'Ea voluptas qui amet animi. Ut quasi magni quis. Ea deleniti est perferendis. Qui inventore rerum repudiandae sed excepturi.', 1, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(12, 149, 'Heloise Anderson I', 'Qui voluptatem qui optio maiores consequatur necessitatibus. Aperiam blanditiis laudantium qui placeat.', 2, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(13, 123, 'Bernice Klein MD', 'Illum ut et corporis error eligendi iusto. Quod id sint ut rerum. Fugit nihil nemo iusto aliquam voluptatum eum omnis. Sunt aut possimus ipsa dignissimos aut eaque.', 5, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(14, 149, 'Tyree Greenholt DVM', 'Ipsum occaecati et eligendi quia reiciendis. Ex in est voluptatem et aut deserunt voluptatibus. Ipsam quibusdam enim ut est corrupti fuga aperiam qui. Sequi sunt molestias sed maiores.', 1, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(15, 130, 'Cicero Donnelly', 'Ex architecto eos ut provident dolor incidunt enim. Aut illo assumenda corrupti ut ipsa aspernatur quo. Reiciendis esse eligendi dolore enim voluptas iure enim.', 0, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(16, 127, 'Waylon Stokes', 'Et dolorem non rerum. Aut distinctio vitae aut nulla voluptates. Et animi explicabo rerum laborum quibusdam repellendus optio. Eum doloribus velit reiciendis.', 1, '2019-12-19 02:30:41', '2019-12-19 02:30:41'),
(17, 149, 'Adah Raynor', 'Quasi ipsa laborum sed rem nemo. Aut quibusdam temporibus quisquam quia velit nesciunt. Cupiditate est itaque dolores quidem doloribus dolore et dolorum.', 1, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(18, 152, 'Madyson Heller', 'Nulla ab incidunt aliquam rerum libero id ratione rerum. Voluptates inventore ut voluptas eveniet. Vero nostrum temporibus sint inventore voluptatem. Labore magnam enim ut dolorum eaque et.', 3, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(19, 125, 'Briana Watsica', 'Aut ex velit eos libero ex. Asperiores qui numquam accusantium rerum aut autem recusandae. Sint minus modi delectus veritatis nemo nisi quis.', 5, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(20, 128, 'Prof. Janick Prosacco', 'Non et error illum. Quidem et nobis tenetur et. Et sed tenetur et nobis ratione. Repellat veniam quis qui ratione consequuntur.', 1, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(21, 137, 'Elmira Ryan II', 'Quae cupiditate accusamus nesciunt alias aperiam excepturi quaerat odit. Explicabo qui aliquam harum ea est est dolore. Velit sed quia eos illum. Quam sed dolore molestiae vel nemo a sunt eveniet.', 5, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(22, 132, 'Stephon Beatty', 'Voluptas qui beatae maxime. Totam aut sint voluptatem dicta ipsum. Laborum occaecati repellat possimus unde soluta.', 5, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(23, 153, 'Cristopher Terry', 'Voluptatum nam voluptas et dolores aut. Est nisi perferendis deserunt est officiis ratione.', 4, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(24, 137, 'Prof. Buford Legros', 'Quia expedita est asperiores asperiores qui saepe non. Ipsam aspernatur enim ut voluptatem non est ducimus non. Fugit cum eveniet voluptate assumenda ut.', 5, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(25, 153, 'Arielle Sanford', 'Magnam ut corporis praesentium perferendis voluptas et neque. Aliquam deserunt laborum et velit tempore at. Similique fuga qui ullam sunt est id. Libero at rem a odio rerum.', 2, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(26, 155, 'August Jaskolski', 'Rem fugiat enim minima dicta dolorem et veritatis. Vitae et quo error aut enim sint quo enim. Incidunt magni dignissimos consequuntur iure facere voluptatem et. Ut ipsa ipsam consequatur.', 1, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(27, 138, 'Jewell Kreiger II', 'Aliquam nisi rem nihil nobis animi officiis vitae. Accusamus blanditiis asperiores veniam sit et molestiae beatae neque. Animi enim ut corporis deleniti dolor. Numquam dolorem quis quia perferendis.', 2, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(28, 154, 'Lew Crona', 'Consequatur quo commodi adipisci incidunt. Facilis et quibusdam esse maxime facere mollitia. Dignissimos et velit sit sed.', 1, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(29, 160, 'Agustina Kovacek', 'Minus dolor ducimus eius ea facere assumenda perferendis. Consequatur fuga est sed. Omnis perspiciatis sed dolorem sunt dolores.', 0, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(30, 126, 'Laurel Sauer', 'Voluptas nihil nesciunt nihil magnam corrupti et laboriosam. Aut omnis voluptas aliquid aliquid explicabo molestiae quo. Consectetur atque aut non.', 5, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(31, 122, 'Emelia Fisher', 'Distinctio fugit maiores aspernatur sed. Aut corrupti deserunt molestiae recusandae voluptatem. Quia et quidem tenetur.', 3, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(32, 126, 'Charity Bernhard', 'Voluptatibus qui aut voluptatibus dolores a. Dignissimos nobis omnis eos. Voluptates sit tenetur magnam est.', 2, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(33, 156, 'Zula Bogisich', 'Repudiandae placeat accusantium ut nobis. Rem in omnis tempore blanditiis minima distinctio. In quaerat voluptas facere voluptatem non impedit consequatur.', 2, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(34, 142, 'Korey West', 'Ad sunt eius est dicta labore fugiat similique. Nam animi eos pariatur omnis. Veritatis ipsum autem velit sit. Corporis repudiandae totam vel culpa voluptas qui ad molestias. Earum vel est enim alias ea delectus optio.', 3, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(35, 146, 'Kacey Gerlach', 'Sequi similique harum quidem et aut ad magni. Et vero qui natus voluptate culpa ratione nobis laudantium. Sit eum provident ratione suscipit labore. Eos saepe molestiae quam aut harum at. Labore blanditiis facilis aut fugiat nam ad.', 2, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(36, 134, 'Prof. Viviane O''Conner MD', 'Aut et nisi optio modi. Sapiente sequi et corrupti itaque impedit exercitationem veritatis. Nihil magnam sit et est nostrum numquam. Autem eos alias iste voluptatem atque odit nobis. Quaerat voluptas consequatur sint accusamus accusamus aut.', 0, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(37, 135, 'Ms. Rafaela Goldner IV', 'Necessitatibus illo optio soluta quisquam. Aut minus aperiam illum est quas. Minima distinctio consequatur ex voluptas quis.', 3, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(38, 131, 'Marilie Schiller', 'Quia consequatur rerum fugit totam soluta natus hic. Quia minima rerum quaerat. Ut et fugiat harum aut iusto eaque. Quo accusantium maiores ex nemo optio ex. Veniam minus nostrum aut dolore ipsum sapiente.', 0, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(39, 137, 'Matteo Schinner', 'Voluptates sunt qui magni in. Iste nisi dolorum molestias vel minus nobis. Perspiciatis commodi consequatur quibusdam reiciendis ut harum provident impedit.', 5, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(40, 160, 'Julia Pagac', 'Non inventore voluptatem doloremque consequuntur placeat voluptatum rerum. Sit provident at id ut aperiam. Cupiditate quam officia dolor est excepturi voluptas. Possimus nulla necessitatibus qui sunt est earum aut. Nisi asperiores est enim tempore necessitatibus sint consequatur.', 0, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(41, 125, 'Prof. Herta Williamson MD', 'Doloremque expedita sunt cumque sapiente voluptatem. Similique ut nesciunt omnis quae dolor autem et. Ea incidunt amet quae quisquam.', 5, '2019-12-19 02:30:42', '2019-12-19 02:30:42'),
(42, 156, 'Wilbert McGlynn', 'Ut odit iste culpa corporis. Quis mollitia et voluptatem. Facilis neque consequatur dolor dolorum eius eveniet perspiciatis voluptas.', 1, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(43, 128, 'Lydia Cruickshank', 'Accusamus voluptatum eos animi est suscipit ullam. Et non rerum in et ipsa sed aut ut. Ea voluptas eaque et ut.', 0, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(44, 160, 'Mr. Godfrey Ratke III', 'Quas totam delectus sunt nulla deleniti. Harum consequatur aliquam aut maxime non molestiae. Ad ipsam ut eligendi et vitae.', 0, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(45, 144, 'Garrick Haley', 'Enim saepe distinctio et libero voluptas. Doloribus debitis quo quidem sit magni. Molestias maxime tenetur saepe nisi. Voluptatem dolor fugiat officia sit incidunt aut.', 5, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(46, 136, 'Ms. Reanna Rohan', 'Voluptatem rerum quidem voluptates et. Eligendi quo et eligendi est esse est et repellat. Enim dolores non rerum et. Vitae numquam sunt cum. Natus ut repellendus ullam ut aut similique.', 1, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(47, 128, 'Mr. Adalberto Monahan', 'At molestiae neque recusandae qui sequi distinctio velit. Adipisci laborum nostrum ut nam. Dolores repudiandae est necessitatibus sint. Est sequi aperiam rerum et dolorem nulla.', 3, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(48, 141, 'Patricia Moen PhD', 'Voluptatibus id atque sed eos itaque natus laboriosam occaecati. Laboriosam velit consequatur eum vero vitae. Reprehenderit saepe facilis iste sunt blanditiis labore necessitatibus. Occaecati in in error harum excepturi et. Nostrum aut ad earum odio.', 0, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(49, 136, 'Mr. Izaiah Klein', 'Ex nobis aliquam est labore nulla ut. Non sed totam odit quia deleniti ut culpa. Similique rerum ipsa consequatur minus molestias.', 3, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(50, 133, 'Mrs. Vida Howell', 'Odio rerum excepturi quos qui. In commodi excepturi eum saepe. Est tempora aut veniam ex distinctio. Est officiis qui magni dolorem.', 0, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(51, 132, 'Dr. Terry Farrell DDS', 'Animi suscipit voluptates accusantium quas reprehenderit. Et omnis minima quia a omnis. Voluptatem eius dolor repellendus.', 1, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(52, 131, 'Dr. Harold Gerlach', 'Enim dolores quisquam itaque nisi eveniet. Nobis et alias laboriosam commodi voluptas. Dignissimos quidem et impedit veritatis dolores.', 2, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(53, 160, 'Hector Wehner', 'Doloremque nostrum enim sunt deserunt id maiores voluptatem. Perspiciatis et consequatur voluptates eum cupiditate ut. Unde dolorem aut eos.', 1, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(54, 127, 'Dr. Camron Eichmann Jr.', 'Debitis placeat in expedita voluptas. Voluptatibus ipsum ex quia sit.', 1, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(55, 143, 'Miss Kirsten Schulist I', 'Voluptas nemo id voluptatum blanditiis deleniti labore ex et. Ipsam culpa voluptate voluptas cupiditate. Nulla tenetur quos enim consectetur.', 2, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(56, 158, 'Shirley Hartmann', 'Esse facere a est repellat occaecati architecto voluptas. Qui quos deleniti voluptas enim et aspernatur esse. Officia sint eius delectus nihil id. Distinctio doloremque dolor sed a numquam et et nihil. Doloremque voluptatem itaque asperiores iste minima similique similique.', 0, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(57, 133, 'Enos Marquardt', 'Officiis sit alias magni velit sit saepe et. Nulla nesciunt quas ut assumenda atque quisquam aspernatur. Aut repellat qui nemo consequuntur architecto sint assumenda. Inventore repellendus rerum sapiente voluptatem itaque corporis nihil.', 3, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(58, 155, 'Elliott Jast', 'Rerum cupiditate illo qui facilis consectetur mollitia. Facere praesentium explicabo accusamus maxime eum debitis dolore. Deleniti dolorum deserunt veritatis in hic voluptates repellat.', 2, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(59, 135, 'Dr. Christy Price DVM', 'Similique id consequatur aut doloribus cupiditate ratione. Quia necessitatibus non sit cum illo perspiciatis. Tenetur iusto id laboriosam dicta. Beatae quo aut blanditiis omnis consequatur laborum. Nesciunt facilis porro consequatur possimus.', 5, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(60, 159, 'Prof. Seamus Hessel', 'Quas quasi asperiores atque quae consectetur. Rerum nesciunt ullam esse aut. Ab ducimus assumenda quia sed. Ipsam voluptas et accusantium beatae possimus.', 0, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(61, 160, 'Mrs. Arielle Marks V', 'Et rem repellendus voluptatibus. Aut mollitia rerum quo similique reprehenderit. Aut nostrum vel vel non sint. Unde nostrum beatae voluptatem nostrum illum.', 5, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(62, 137, 'Gladys Kovacek', 'Consequuntur quam ea molestias asperiores eaque. Earum maiores neque qui ex ut aut nihil.', 3, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(63, 143, 'Art Jones', 'Exercitationem dolor doloribus in necessitatibus aut et. Sed ea enim eligendi nostrum et laboriosam quisquam eius. Molestiae unde cum perspiciatis ex est impedit. Eius quas sit expedita ut laboriosam sunt.', 4, '2019-12-19 02:30:43', '2019-12-19 02:30:43'),
(64, 146, 'Horacio Hand', 'Laboriosam eveniet similique omnis libero ea cupiditate illum. Accusamus qui quae est animi qui. Dignissimos possimus blanditiis nobis sed dolorem esse odit molestiae. Iure eum consequatur nulla libero minima magnam.', 2, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(65, 143, 'Jimmie Shields Sr.', 'Officia at totam voluptatem vitae. Eum magnam error error reiciendis.', 5, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(66, 126, 'Dr. Caterina Renner Sr.', 'Quia libero voluptatem magnam inventore aut aut et. Laboriosam aliquam et autem in autem voluptas. Nobis dolorem minus aut dicta minus accusamus. Excepturi ipsam rerum voluptatem consectetur deleniti deserunt.', 5, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(67, 126, 'Trever Lindgren', 'Minus consequatur ut voluptate molestias voluptas et. Est est iste dolores laborum quasi. Sunt deserunt pariatur omnis cumque rerum voluptatem. Quibusdam ducimus ipsum assumenda repudiandae temporibus.', 3, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(68, 132, 'Dimitri Abshire DVM', 'Et voluptas veniam sunt neque sit rerum. Non facilis et modi. Id in modi delectus est. Ut earum beatae et voluptatem fugit asperiores. Quaerat sint qui sequi possimus temporibus voluptatibus omnis.', 1, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(69, 154, 'Vladimir Flatley DVM', 'Maxime est error voluptatem maxime fuga non magnam. Omnis est illo non fugiat nihil nihil. Quis culpa provident dolorum explicabo praesentium sint consequatur.', 3, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(70, 160, 'Freida Bashirian', 'Dolorum asperiores praesentium tempora quaerat ullam sed. Suscipit ea eos adipisci cumque aut error. Itaque aut rerum quis laborum magni consequatur id.', 4, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(71, 158, 'Miss Kenna Vandervort PhD', 'Aliquid et velit dolor voluptates soluta sequi neque. Non repellendus qui pariatur nemo. Vel ipsum aut voluptatum qui ducimus.', 3, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(73, 148, 'Khalid Anderson V', 'Illo voluptatem doloremque omnis blanditiis blanditiis ut. Est quo laudantium voluptatem omnis consequatur consequatur dolor. Qui impedit ut et id ut. Commodi doloribus nostrum nesciunt odit.', 5, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(74, 154, 'Prof. Monica Bergstrom', 'Excepturi fuga et excepturi eos qui ut. Laborum voluptas consectetur modi eos aut. Aliquid consequatur pariatur aut voluptatibus totam.', 5, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(75, 156, 'Lia Balistreri', 'Rem maiores id et hic et et aut vel. Facilis commodi quia nesciunt id aut consectetur. Reprehenderit assumenda magnam excepturi minus.', 3, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(76, 154, 'Carol Balistreri MD', 'Incidunt veniam quidem aut animi blanditiis iste. Porro voluptates cupiditate quasi sed voluptatem est. Minima itaque sunt in enim impedit aut sunt. Voluptatum dolorem rem rerum tempore unde facere laboriosam.', 5, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(77, 131, 'Miss Tracy Murazik', 'Illum consequatur voluptas voluptas animi libero. Voluptate pariatur molestias sed architecto. Repellendus aut qui molestiae cumque ut.', 1, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(78, 130, 'Nelson Cremin', 'Vel veniam voluptatem ea saepe nisi sed est. Laudantium officia consectetur tenetur corrupti assumenda. Vitae velit ipsa blanditiis unde.', 3, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(79, 149, 'Christopher Lakin I', 'Aperiam voluptatem nam vitae ullam et omnis aut quo. Nulla qui tempore nisi et eveniet quis. Eum ex rerum dignissimos.', 1, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(80, 157, 'Elmore Leannon', 'Deleniti voluptas molestias blanditiis illo perspiciatis. Eius et excepturi aliquam doloribus qui. Voluptatem repellendus dolor labore animi natus. Velit accusantium aut assumenda est quod.', 5, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(81, 137, 'Mathew Abernathy III', 'Consequuntur omnis eveniet aliquid velit sed vel. Porro vel facere qui facilis quos molestias expedita. Quae voluptate harum quam. Ex amet molestiae quaerat quis repellat labore.', 3, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(82, 148, 'Madison Zieme', 'Laudantium accusantium sunt ducimus qui fuga rerum. Aut voluptates et ut ea ut. Quod fugit sit vitae nemo qui provident accusantium et. Recusandae veritatis ab ducimus at eos magni fuga cum.', 0, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(83, 159, 'Enrico Jakubowski', 'Nobis voluptatem temporibus expedita aliquid itaque sunt nulla. Vel earum ea porro autem et quae unde. Dolores at hic asperiores est dolor eos modi. Et et alias nesciunt.', 3, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(84, 132, 'Edison Konopelski', 'Odit excepturi et quo enim esse asperiores. Consequuntur debitis at aut. Recusandae natus delectus culpa molestiae nulla sed et.', 5, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(85, 141, 'Bella Toy', 'Suscipit quas magni iste natus assumenda. Dolor qui occaecati similique voluptatibus corrupti. Ab porro sint qui rerum voluptatem rerum. Maxime quidem nulla occaecati suscipit delectus in. Commodi consequuntur aut sunt sapiente adipisci placeat ut.', 0, '2019-12-19 02:30:44', '2019-12-19 02:30:44'),
(86, 133, 'Francis Wiza', 'Ex quia dolorem beatae nihil. Quidem odit illum quasi id repellat consequatur. Suscipit aut et quis modi quia eos.', 0, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(87, 129, 'Prof. Beulah Sanford DVM', 'Alias enim qui dolorum modi delectus et eius animi. Dignissimos sit et nobis ducimus. Sunt perferendis tempore laborum fugiat. Consequuntur mollitia ut ea itaque. Et incidunt possimus omnis molestiae.', 2, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(88, 157, 'Libbie Carter', 'Vitae esse dignissimos natus quasi. Nisi accusamus itaque repellendus quod placeat. Omnis voluptas facere pariatur.', 0, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(89, 122, 'Ashly Keebler IV', 'Eaque qui dolore expedita cum id et saepe. Quisquam quod nulla dicta id veritatis est. Dolor eveniet vel quis vel et et magnam. Sint pariatur et vitae qui et maiores cupiditate sed.', 2, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(90, 147, 'Justice Mills', 'Quis doloribus dolore quia qui voluptatem excepturi quod. In fugit laborum neque. Debitis deserunt dolores alias ipsa architecto laboriosam eum labore. Repellendus quos voluptas qui consequatur omnis repellendus eligendi.', 4, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(91, 138, 'Stephania Lakin', 'Ut amet modi explicabo rem eos. Amet minima laboriosam sapiente non ipsam laborum similique. Et temporibus deleniti ut.', 3, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(92, 160, 'Maxie Mante', 'Aperiam sit at optio delectus laudantium. Nam eos quia consectetur quia veniam in labore. Est repellat ut eos consequatur aut molestiae. Quam illum perspiciatis officia voluptatem fugit iusto architecto sit.', 5, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(93, 124, 'Garfield Stoltenberg', 'Et odio illum aliquam dolor error quam. Voluptates cumque quibusdam aut dolorem voluptas nam fugiat.', 5, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(94, 158, 'Dr. Diego Hettinger DDS', 'Ut iure illum rerum nulla. Cupiditate officiis dolor tenetur nostrum pariatur sunt.', 1, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(95, 149, 'Dorothy Quitzon', 'Deserunt commodi in a consequuntur. Quam temporibus facilis dolores totam soluta eos omnis. Delectus reiciendis molestiae ut dolore dolor neque iusto qui. Porro officia soluta quo omnis dolores sint vel voluptatem. Corrupti dolores molestias dolores non ut sed sint.', 1, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(96, 158, 'Ceasar Rodriguez', 'Dignissimos amet necessitatibus nisi molestias aut doloremque. Molestiae in et eum. Et cupiditate et et quia necessitatibus vitae eum.', 5, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(97, 126, 'Kenton Predovic', 'Tempora omnis expedita et dignissimos mollitia sint. Natus enim odio animi officia. Earum natus quasi officiis deserunt iure repellat. Ut fugiat illum possimus possimus aspernatur commodi quisquam.', 5, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(98, 148, 'Claudine Johns', 'Saepe fugiat excepturi rerum necessitatibus. Est expedita optio accusantium. Beatae numquam excepturi possimus sed ipsa culpa eius. Consequatur id cupiditate voluptates perferendis molestiae eum.', 4, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(99, 123, 'Miguel Robel', 'Suscipit aut voluptas dolorem. Assumenda vitae adipisci itaque ducimus.', 1, '2019-12-19 02:30:45', '2019-12-19 02:30:45'),
(100, 160, 'Myriam Vandervort', 'Quo qui amet aspernatur explicabo optio. Numquam et nihil omnis enim et. Quia libero aliquam sapiente quaerat.', 3, '2019-12-19 02:30:45', '2019-12-19 02:30:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sameer', 'sameer@gmail.com', NULL, '$2y$10$GotRX9.O0GZIn5/6m48L0edB6r3pLWDUOOahrqUbZqJKaRRXKd53C', NULL, '2019-12-20 01:03:41', '2019-12-20 01:03:41'),
(2, 'abhash', 'abhash@gmail.com', NULL, '$2y$10$9tCFpUqiqWpC8gccEfDvc.qH2ICclEVQwf7nfKjJcK0..djZ1UI4i', NULL, '2019-12-20 01:04:17', '2019-12-20 01:04:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_product_id_index` (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
